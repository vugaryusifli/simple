import React, {useState} from "react";
import styled from "styled-components";
import {connect} from "react-redux";
import {register, login} from "../../../Redux/actions/user";

function Login(props) {

    const [data, setData] = useState({
        name: "",
        pass: ""
    });

    function onChangeInput(e) {
        setData({ ...data, [e.target.name] : e.target.value });
    }

    async function onRegister() {
        try {
            await props.register(data);
        }
        catch (e) {
            console.log(e.response.data);
        }
    }

    async function onLogin() {
        try {
            await props.login(data);
        }
        catch (e) {
            console.log(e.response.data);
        }
    }

    return (
        <Form>
            <input onChange={onChangeInput} type="text" name="name" value={data.name}/>
            <input onChange={onChangeInput} type="password" name="pass" value={data.pass}/>
            <button type="button" onClick={onLogin}>Login</button>
            <button type="button" onClick={onRegister}>Register</button>
        </Form>
    )
}

const Form = styled.form`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

export default connect(null, {register, login})(Login);