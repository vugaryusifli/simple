import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Route, Redirect} from "react-router-dom";

function UserOffline({ user, component: Component, ...rest }) {
    console.log(!user.name);
    return (
        <Route
            {...rest}
            render={props => !user.name ? <Component {...props}/> : <Redirect to="/" /> }
        />
    );
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(UserOffline);