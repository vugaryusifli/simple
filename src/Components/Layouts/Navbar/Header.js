import React from "react";
import styled from "styled-components";
import logo from "../../../img/logo.svg";
import Menu from "./NavbarMenu/Menu";

function Header(props) {
    return (
        <Container>
            <Box>
                <Image src={logo} alt="Logo"/>
            </Box>
            <Menu/>
        </Container>
    );
}

const Container = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 90px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #AEC6CF;
  padding: 0 2rem;
  z-index: 1;
`;

const Box = styled.div`
  display: flex;
  align-items: center;
`;

const Image = styled.img`
  max-width: 50px;
`;

export default Header;