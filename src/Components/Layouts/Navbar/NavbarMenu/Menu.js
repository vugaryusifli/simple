import React from "react";
import styled from "styled-components";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import {logOut} from "../../../../Redux/actions/user";

function Menu(props) {
    return (
        <NavMenuItems>
            {
                Object.keys(props.user).length !== 0 ?
                    <>
                        <NavMenuItem>
                            <NavMenuLink exact to="/">Home</NavMenuLink>
                        </NavMenuItem>
                        <NavMenuItem>
                            <NavMenuLink exact to="/about">About Us</NavMenuLink>
                        </NavMenuItem>
                        <NavMenuItem>
                            <NavMenuLink exact to="/services">Services</NavMenuLink>
                        </NavMenuItem>
                        <NavMenuItem>
                            <NavMenuLink exact to="/contact">Contact Us</NavMenuLink>
                        </NavMenuItem>
                        <button type="button" onClick={() => props.logOut()}>
                            Log Out
                        </button>
                    </>
                    :
                    <NavMenuItem>
                        <NavMenuLink exact to="/auth/login">Login</NavMenuLink>
                    </NavMenuItem>
            }
        </NavMenuItems>
    );
}

const NavMenuItems = styled.ul`
  display: flex;
  list-style: none;
`;

const NavMenuItem = styled.li`
  &:not(:last-child) {
    margin-right: 1.2rem;
  }
`;

const NavMenuLink = styled(NavLink)`
    &.active {
        background-color: #000 !important;
        color: #fff !important;
    }
    text-decoration: none;
    background-color: #EEE;
    color: #444;
    font-weight: 700;
    font-size: 1.2em;
    padding: .1rem .8rem;
    border-radius: 10px;
    transition: all .3s;
    &:hover {
      background-color: #000;
      color: #fff;
    }
`;

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, {logOut})(Menu);