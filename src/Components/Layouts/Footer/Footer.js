import React from "react";
import styled from "styled-components";

function Footer() {
    return (
        <Container>
            <div>Left</div>
            <div>Right</div>
        </Container>
    );
}

const Container = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 90px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #AEC6CF;
  padding: 0 2rem;
  z-index: 1;
`;

export default Footer;