import React from "react";
import styled from "styled-components";
import error404 from "../../img/error404.jpg";
import {Link} from "react-router-dom";

function Error(props) {

    function goBack() {
        props.history.goBack();
    }

    return (
        <Container>
            <Box>
                <Image src={error404} alt="Error" />
                <button onClick={goBack}>Go back</button>
            </Box>
        </Container>
    );
}

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const Box = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
`;

const GoBackLink = styled(Link)`
  position: absolute;
  width: auto;
  bottom: 25px;
  text-align: center;
  text-decoration: none;
  color: #fff;
  border: 1px solid rgba(255,255,255, .2);
  background-color: transparent;
  transition: .3s;
  padding: .5rem;
  font-size: 1.5em;
  &:hover {
    background-color: #000;
    color: #fff;
  }
`;

const Image = styled.img`
  max-width: 500px;
`;

export default Error;