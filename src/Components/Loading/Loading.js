import React from "react";
import styled from "styled-components";
import loading from "../../img/loading.svg";

function Loading() {
    return (
        <Container>
            <Svg src={loading} alt=""/>
        </Container>
    )
}

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
`;

const Svg = styled.img`
  max-width: 200px;
`;

export default Loading;