import React, {Fragment, lazy, Suspense, useEffect, useState} from 'react';
import styled from "styled-components";
import {Route, Switch} from "react-router-dom";
import Header from "../Layouts/Navbar/Header";
import Loading from "../Loading/Loading";
import Error from "../ErrorPage/Error";
import Footer from "../Layouts/Footer/Footer";
import Login from "../Auth/LoginPage/Login";

import UserOnline from "../Auth/UserOnline";
import UserOffline from "../Auth/UserOffline";

// const Home = React.lazy(
//     () =>
//         new Promise((resolve, reject) =>
//             setTimeout(() => resolve(import("../HomePage/Home")), 500000)
//         )
// );

// const Home = lazy(() => new Promise(resolve => resolve(import("../HomePage/Home"))));
// const About = lazy(() => new Promise(resolve => resolve(import("../AboutPage/About"))));
// const Services = lazy(() => new Promise(resolve => resolve(import("../ServicesPage/Services"))));
// const Contact = lazy(() => new Promise(resolve => resolve(import("../ContactPage/Contact"))));

const Home = lazy(() => import("../HomePage/Home"));
const About = lazy(() => import("../AboutPage/About"));
const Services = lazy(() => import("../ServicesPage/Services"));
const Contact = lazy(() => import("../ContactPage/Contact"));

function App(props) {

    return (
        <Fragment>
            <Header/>
            <Container>
                <Suspense fallback={<Loading/>}>
                    <Switch>
                        <UserOnline exact location={props.location} path="/" component={Home}/>
                        <UserOnline exact location={props.location} path="/about" component={About}/>
                        <UserOnline exact location={props.location} path="/services" component={Services}/>
                        <UserOnline exact location={props.location} path="/contact" component={Contact}/>
                        <UserOffline exact location={props.location} path="/auth/login" component={Login}/>
                        <Route exact path="/*" component={Error} />
                    </Switch>
                </Suspense>
            </Container>
            <Footer/>
        </Fragment>
    );
}

const Container = styled.div`
  margin-top: 90px;
  margin-bottom: 90px;
`;

export default App;
