export default function user(state = {}, action = {}) {
    switch (action.type) {
        case "ADD_USER":
            console.log(action.user);
            return action.user;
        case "REMOVE_USER":
            return {};
        default:
            return state;
    }
}