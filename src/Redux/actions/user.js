import axios from "axios";

export const register = (data) => async (dispatch) => {
    let user = await axios.post("http://192.168.1.107:4444/api/register", data)
        .then(res => res.data.user);
    localStorage.setItem("token", user.token);
    dispatch(registerDispatch(user));
};

export const login = (data) => async (dispatch) => {
    let user = await axios.post("http://192.168.1.107:4444/api/login", data)
        .then(res => res.data.user);
    localStorage.setItem("token", user.token);
    dispatch(registerDispatch(user));
};

export const registerDispatch = (user) => ({
    type: "ADD_USER",
    user
});

export const logOut = () => (dispatch) => {
    localStorage.removeItem("token");
    dispatch(logOutDispatch());
};

const logOutDispatch = () => ({
    type: "REMOVE_USER"
});